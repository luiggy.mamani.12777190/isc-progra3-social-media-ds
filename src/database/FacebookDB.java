package database;

import social_media.Facebook;
import social_media.User;

public class FacebookDB {

    private Facebook facebook;

    public FacebookDB() {
        this.facebook = new Facebook();
    }

    public void runDB() {
        User daniel = new User("daniel@gmail.com","1234","Daniel", true);
        User thomas = new User("tomas@gmail.com","1234","Thomas", true);
        User andres = new User("andres@gmail.com","1234", "Andres", true);
        User luiggy = new User("luiggy@gmail.com","1234", "Luiggy", true);
        User santiago = new User("santiago@gmail.com","1234", "Santiago", true);
        User helen = new User("helen@gmail.com", "1234", "Helen", false);
        User kathy = new User("kathy@gmail.com", "1234", "Kathy", false);

        daniel.setPublic(false);
        santiago.setPublic(false);
        kathy.setPublic(false);

        facebook.signUp(daniel);
        facebook.signUp(thomas);
        facebook.signUp(andres);
        facebook.signUp(helen);
        facebook.signUp(luiggy);
        facebook.signUp(santiago);
        facebook.signUp(kathy);

        facebook.joinUsers(daniel, thomas);
        facebook.joinUsers(daniel, andres);
        facebook.joinUsers(daniel, luiggy);

        facebook.joinUsers(helen, luiggy);
        facebook.joinUsers(helen, thomas);
        facebook.joinUsers(luiggy, helen);
        facebook.joinUsers(luiggy, santiago);
        facebook.joinUsers(luiggy, thomas);
        facebook.joinUsers(santiago, andres);
        facebook.joinUsers(kathy, andres);
    }

    public Facebook getFacebook() {
        return facebook;
    }
}