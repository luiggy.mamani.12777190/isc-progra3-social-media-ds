package database;

import social_media.AuthenticatedUser;
import social_media.Twitter;
import social_media.User;

public class TwitterDB {

    private Twitter twitter;

    public TwitterDB() {
        this.twitter = new Twitter();
    }

    public void runDB() {
        User daniel = new User("daniel@gmail.com", "1234", "Daniel", true);
        User thomas = new User("tomas@gmail.com", "1234", "Thomas", true);
        User andres = new User("andres@gmail.com", "1234", "Andres", true);
        User luiggy = new User("luiggy@gmail.com", "1234", "Luiggy", true);
        User santiago = new User("santiago@gmail.com", "1234", "Santiago", true);
        User helen = new User("helen@gmail.com", "1234", "Helen", false);
        User kathy = new User("kathy@gmail.com", "1234", "Kathy", false);

        daniel.setPublic(false);
        santiago.setPublic(false);
        kathy.setPublic(false);

        twitter.signUp(daniel);
        twitter.signUp(thomas);
        twitter.signUp(andres);
        twitter.signUp(helen);
        twitter.signUp(luiggy);
        twitter.signUp(santiago);
        twitter.signUp(kathy);
        AuthenticatedUser.setUser(twitter.getGraph(), daniel);
        twitter.follow(thomas);
        twitter.follow(andres);
        twitter.follow(luiggy);
        AuthenticatedUser.setUser(twitter.getGraph(), helen);
        twitter.follow(luiggy);
        twitter.follow(thomas);
        AuthenticatedUser.setUser(twitter.getGraph(), luiggy);
        twitter.follow(helen);
        twitter.follow(santiago);
        twitter.follow(thomas);
        AuthenticatedUser.setUser(twitter.getGraph(), santiago);
        twitter.follow(andres);
    }

    public Twitter getTwitter() {
        return twitter;
    }
}
