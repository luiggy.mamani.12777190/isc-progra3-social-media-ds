package social_media;

import graph.Edge;
import graph.Graph;

import java.util.LinkedList;

public class AuthenticatedUser {

    public static User user;
    public static LinkedList<Edge<User>> friends;

    public static void setUser(Graph<User> users, User user) {
        AuthenticatedUser.user = user;
        AuthenticatedUser.friends = users.getGraph().get(user);
    }

    public static boolean containsFriend(User userToCompare) {
        for (Edge<User> friend : friends) {
            if (friend.getDestination().equals(userToCompare))
                return true;
        }

        return false;
    }
}