package social_media;

import graph.DirectedGraph;
import graph.Edge;
import graph.Graph;
import view.TwitterView;

import java.util.LinkedList;
import java.util.Map;

public class Twitter extends SocialMedia {
    TwitterView view;
    public Twitter() {
        userNetwork = new DirectedGraph<>();
        view = new TwitterView();
    }

    public void follow(User user) {
        AuthenticatedUser.friends.add(new Edge<>(AuthenticatedUser.user, user));
        user.setAmountFollowers(user.getAmountFollowers() + 1);
        AuthenticatedUser.user.setAmountFollowing(AuthenticatedUser.user.getAmountFollowing() + 1);
    }

    public void unfollow(User user) {
        AuthenticatedUser.friends.remove(new Edge<>(AuthenticatedUser.user, user));
        user.setAmountFollowers(user.getAmountFollowers() - 1);
        AuthenticatedUser.user.setAmountFollowing(AuthenticatedUser.user.getAmountFollowing() - 1);
    }

    public void showUsers() {
        view.showUsers((DirectedGraph) userNetwork);
    }
    public Graph<User> getGraph(){
        return userNetwork;
    }

}
