package social_media;

import graph.Graph;
import graph.UndirectedGraph;

public class SocialMedia {
    protected Graph<User> userNetwork;

    public SocialMedia() {
        userNetwork = new UndirectedGraph<>();
    }

    public void signUp(User newUser) {
        userNetwork.createMainVertex(newUser);
    }

    public void login(String email, String password) {
        userNetwork.getGraph().keySet().forEach(user -> {
            if (user.getEmail().equals(email) && user.getPassword().equals(password))
                AuthenticatedUser.setUser(userNetwork, user);
        });
    }

    public void logout() {
        AuthenticatedUser.setUser(null, null);
    }

    public void joinUsers(User user1, User user2) {
        userNetwork.addEdge(user1, user2);
        user1.setAmountFollowers(user1.getAmountFollowers() + 1);
    }

    /*System.out.println("\t|\tFollowers :" +
            user.getAmountFollowers() +
            "\tFollowing :" +
            user.getAmountFollowing() + "\nFriends:");*/
}