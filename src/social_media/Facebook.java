package social_media;

import graph.Edge;
import graph.UndirectedGraph;
import view.FacebookView;

import java.util.LinkedList;

public class Facebook extends SocialMedia {

    private FacebookView view;

    public Facebook() {
        this.view = new FacebookView();
    }

    public void showUsers() {
        view.showUsers((UndirectedGraph<User>) userNetwork);
    }

}