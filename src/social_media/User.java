package social_media;

public class User {

    private String name, email, password;
    private boolean isPublic, isMale;
    private int amountFollowers, amountFollowing;

    public User (String email, String password, String name, boolean isMale){
        this.name = name;
        this.email = email;
        this.password = password;
        this.isMale = isMale;
        this.isPublic = true;
        this.amountFollowers = 0;
        this.amountFollowing = 0;
    }

    public String getName() {
        return name;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    @Override
    public String toString() {
        return name;
    }


    public String getEmail() {
        return email;
    }


    public String getPassword() {
        return password;
    }


    public int getAmountFollowers() {
        return amountFollowers;
    }

    public void setAmountFollowers(int amountFollowers) {
        this.amountFollowers = amountFollowers;
    }

    public int getAmountFollowing() {
        return amountFollowing;
    }

    public void setAmountFollowing(int amountFollowing) {
        this.amountFollowing = amountFollowing;
    }

    @Override
    public boolean equals(Object obj) {
        User userToCompare = (User) obj;
        return getEmail().equals(userToCompare.getEmail());
    }

    public boolean isMale() {
        return isMale;
    }
}