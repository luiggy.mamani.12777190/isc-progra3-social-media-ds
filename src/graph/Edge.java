package graph;

public class Edge<T> {

    private T source;
    private T destination;

    public Edge(T source, T destination) {
        this.source = source;
        this.destination = destination;
    }

    public T getSource() {
        return source;
    }

    public T getDestination() {
        return destination;
    }

    @Override
    public boolean equals(Object object) {
        Edge<T> edgeToCompare = (Edge<T>) object;
        if (getDestination().equals(edgeToCompare.getDestination())
                && getSource().equals(edgeToCompare.getSource()))
            return true;

        return false;
    }

}