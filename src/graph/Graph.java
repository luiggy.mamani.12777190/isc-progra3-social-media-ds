package graph;

import java.util.HashMap;
import java.util.LinkedList;

public abstract class Graph<T> {

    protected HashMap<T, LinkedList<Edge<T>>> graph;

    public HashMap<T, LinkedList<Edge<T>>> getGraph() {
        if (graph == null)
            graph = new HashMap<>();

        return graph;
    }

    public void createMainVertex(T mainVertex) {
        getGraph().putIfAbsent(mainVertex, new LinkedList<>());
    }

    public void createManyMainVertices(T... mainVertices) {
        for (T head : mainVertices)
            createMainVertex(head);
    }

    protected void addRelation(T mainVertex, T newVertex) {
        if (!hasRelation(mainVertex, newVertex))
            getGraph().get(mainVertex).add(new Edge<>(mainVertex, newVertex));
    }

    protected boolean hasRelation(T headVertex, T newVertex) {
        for (Edge<T> edge : getGraph().get(headVertex))
            if (edge.getDestination().equals(newVertex))
                return true;

        return false;
    }
    public abstract void addEdge(T mainVertex, T newVertex) ;
    protected boolean existsVertex(T vertex) {
        return getGraph().get(vertex) != null;
    }

    public int size() {
        return graph.size();
    }
}