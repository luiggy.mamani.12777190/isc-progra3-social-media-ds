package graph;

public class DirectedGraph<T> extends Graph<T>{
    public void addEdge(T mainVertex, T newVertex) {
        if (!existsVertex(mainVertex) || !existsVertex(newVertex)) {
            createMainVertex(mainVertex);
            createMainVertex(newVertex);
        }

        addRelation(mainVertex, newVertex);
    }
}
