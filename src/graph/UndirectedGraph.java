package graph;

public class UndirectedGraph<T> extends Graph<T>{

    public void addEdge(T mainVertex, T newVertex) {
        if (!existsVertex(mainVertex) || !existsVertex(newVertex)) {
            createMainVertex(mainVertex);
            createMainVertex(newVertex);
        }

        addRelation(mainVertex, newVertex);
    }

    public void addManyEdges(T mainVertex, T... newVertex) {
        for (T vertex : newVertex)
            addEdge(mainVertex, vertex);
    }

}