package view;

import graph.DirectedGraph;
import graph.Edge;
import social_media.AuthenticatedUser;
import social_media.User;

import java.util.LinkedList;
import java.util.Map;

public class TwitterView {
    DirectedGraph<User> userNetwork;

    public void showHeader() {
        System.out.println(Colors.CYAN_BOLD + "-".repeat(40));
        System.out.println("\n" + " ".repeat(15) + "TWITTER\n");
        System.out.println("-".repeat(40) + Colors.CYAN_BOLD + "\n");
    }

    private void showProfile(User user) {
        String profile = Icon.FEMALE;
        if (user.isMale())
            profile = Icon.MALE;
        System.out.println(
                profile + " " + Colors.RESET +
                        user + " : " +
                        (AuthenticatedUser.containsFriend(user) ?
                                Colors.YELLOW + "Following" + Colors.YELLOW :
                                Colors.RED + "Follow" + Colors.RED));

    }

    public void showUsers(DirectedGraph<User> userNetwork) {
        showHeader();
        this.userNetwork = userNetwork;
        userNetwork.getGraph().forEach((user, users) -> {
                    if (!user.equals(AuthenticatedUser.user)) {
                        showProfileUser(user);
                        System.out.println(".".repeat(40) + "\n");
                    }
                }
        );
    }

    public void showContactNetwork(User user) {
        System.out.println(showFollowers(user));
        System.out.println(showFollowing(user));
    }

    private void showProfileUser(User user) {
        showProfile(user);
        System.out.println(Colors.YELLOW_BOLD+user.getAmountFollowers() + " Followers " +
                user.getAmountFollowing() + " Following"+Colors.RESET);
        showStatusAccount(user);
    }

    private void showStatusAccount(User user) {
        if (!user.isPublic()) {
            System.out.print("\uD83D\uDD12" + Colors.RED + " This account is private" + Colors.RESET + "\n");
        } else {
            showContactNetwork(user);
        }
    }

    public String showFollowers(User user) {
        String tittle = Colors.PURPLE + "--- Followers ---" + Colors.RESET;
        String followers = tittle;

        for (Map.Entry entry : userNetwork.getGraph().entrySet()) {
            if (((LinkedList) entry.getValue()).contains(new Edge<>(entry.getKey(), user))) {
                followers += "\n" + entry.getKey();
            }
        }
        return followers.equals(tittle) ? Colors.PURPLE + "Without Followers" + Colors.RESET : followers;
    }

    public String showFollowing(User user) {
        String tittle = Colors.BLUE + "--- Following ---" + Colors.RESET;
        String following = tittle;
        for (Edge edge : userNetwork.getGraph().get(user)) {
            following += "\n" + edge.getDestination();
        }
        return following.equals(tittle) ? Colors.BLUE + "Does Not Following Anyone" + Colors.RESET : following;
    }
}
