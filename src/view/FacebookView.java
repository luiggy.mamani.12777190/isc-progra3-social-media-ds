package view;

import graph.Edge;
import graph.Graph;
import graph.UndirectedGraph;
import social_media.AuthenticatedUser;
import social_media.User;

import java.util.LinkedList;

public class FacebookView {

    private int friendsCommon;

    public FacebookView() {
        this.friendsCommon = 0;
    }

    private void showHeader() {
        System.out.println(Colors.CYAN_BOLD + "-".repeat(40));
        System.out.println("\n" + " ".repeat(15) + "FACEBOOK\n");
        System.out.println("-".repeat(40) + Colors.CYAN_BOLD + "\n");
    }

    private void showProfile(User user) {
        String profile = Icon.FEMALE;
        if (user.isMale())
            profile = Icon.MALE;

        System.out.println(
                profile + " " + Colors.RESET +
                        user + " : " +
                        (AuthenticatedUser.containsFriend(user) ?
                                Colors.YELLOW + "Friend" + Colors.YELLOW :
                                Colors.RED + "Add friend" + Colors.RED));
    }

    private void showProfileUser(User user, LinkedList<Edge<User>> friends) {

        friendsCommon = 0;
        showProfile(user);

        if (friends.isEmpty())
            System.out.println(Colors.GREEN + "Without friends");
        else {
            System.out.println(Colors.GREEN);
            friends.forEach((friend) -> {
                if (AuthenticatedUser.containsFriend(friend.getDestination()))
                    friendsCommon++;
                if (user.isPublic())
                    System.out.println("-" + friend.getDestination().getName());
            });
        }

        showStatusAccount(user);
    }

    private void showStatusAccount(User user) {
        if (!user.isPublic())
            System.out.print(Icon.LOCK + Colors.RED + " This account is private" + Colors.RED);
    }

    private void showFriendsCommon() {
        System.out.print("\n"+  Colors.RESET +" friends common :  " + friendsCommon + "\n");
    }

    public void showUsers(UndirectedGraph<User> userNetwork) {
        showHeader();

        userNetwork.getGraph().forEach((user, friends) -> {
            if (!user.equals(AuthenticatedUser.user)) {
                showProfileUser(user, friends);
                showFriendsCommon();
                System.out.println(".".repeat(40) + "\n");
            }
        });
    }
}